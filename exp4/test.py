import pickle
import numpy as np
import pandas as pd
from tqdm import tqdm

import torch
from torch.utils.data import DataLoader

import util
from vocab import vocab
from model import Decoder


class Dataset:
    def __init__(self):
        df = pd.read_csv('../raw/test.csv')
        with open('../raw/test_img256.pkl', 'rb') as f:
            self.features = pickle.load(f)
        self.img_ids = df['img_id'].tolist()

    def __len__(self):
        return len(self.img_ids)

    def __getitem__(self, idx):
        img_id = self.img_ids[idx]
        img_ebd = self.features[img_id]
        img_bed = torch.FloatTensor(img_ebd)
        return img_id, img_ebd


test_set = Dataset()
test_loader = DataLoader(test_set, batch_size=256)

device = 'cuda'
decoder = torch.load('log/2018.12.01-21:17:52/model.pth')
decoder = decoder.to(device)
decoder.eval()

result = []
with tqdm(total=len(test_set), ascii=True) as pbar:
    for img_id_b, img_emb_b in iter(test_loader):
        img_ebd_b = img_emb_b.float().to(device)
        predict_b = decoder.evaluate(img_ebd_b).cpu()
        for img_id, predict in zip(img_id_b, predict_b):
            predict = vocab.wordify(predict.numpy().tolist())
            if predict[0] == '<sos>':
                predict = predict[1:]
            if predict[-1] == '<eos>':
                predict = predict[:-1]
            result.append({
                'img_id': img_id,
                'caption': ' '.join(predict)
            })
            pbar.update(1)

result = pd.DataFrame(result)
result.to_csv('./pred.csv', index=False, columns=['img_id', 'caption'])
