import pickle
import pandas as pd
from collections import defaultdict

class Vocab:
    def __init__(self):
        self.word2idx = dict()
        self.idx2word = dict()
        with open('../raw/vocab.pkl', 'rb') as f:
            words = pickle.load(f)
        # <pad> should have value 0
        for word in ['<pad>', '<sos>', '<eos>', '<ukn>']:
            self.add_word(word)

        captions = pd.read_csv('../raw/train.csv')['caption']
        captions = ' '.join(captions.tolist())
        captions = captions.split(' ')

        word2cnt = defaultdict(int)
        for word in captions:
            word2cnt[word] += 1
        for word, cnt in word2cnt.items():
            if cnt >= 20:
                self.add_word(word)

    def add_word(self, word):
        idx = len(self.word2idx)
        self.word2idx[word] = idx
        self.idx2word[idx] = word

    def __len__(self):
        return len(self.word2idx)

    def __call__(self, words):
        '''
        Args:
            words: (list of str) sized [N]
        Return:
            indices: (list of int) sized [N]
        '''
        ukn_idx = self.word2idx['<ukn>']
        return [self.word2idx.get(word, ukn_idx) for word in words]

    def wordify(self, indices, early_break=True):
        '''
        Args:
            indices: (list of int) sized [N]
            early_break: (bool) whether to break when encounter <eos>, default to True
        Return:
            words: (list of str) sized [N]
        '''
        words = []
        for idx in indices:
            assert 0 <= idx < len(self.idx2word)
            word = self.idx2word[idx]
            words.append(word)
            if word == '<eos>' and early_break:
                break
        return words

vocab = Vocab()

if __name__ == '__main__':
    print(len(vocab))
