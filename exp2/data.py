import pickle
import numpy as np
import pandas as pd
from pathlib import Path

import torch
from torch.utils.data import DataLoader
from torch.nn.utils.rnn import pad_sequence

from vocab import vocab

class Dataset:
    def __init__(self):
        with open('../raw/train_img256.pkl', 'rb') as f:
            self.features = pickle.load(f)
        dataframe = pd.read_csv('../raw/train.csv')
        self.anns = dataframe.to_dict(orient='records')

    def __len__(self):
        return len(self.anns)

    def __getitem__(self, idx):
        '''
        Return
            feature: (FloatTensor) sized [256]
            caption: (LongTensor) sized [S]
        '''
        img_name = self.anns[idx]['img_id']
        caption = self.anns[idx]['caption'].split()

        feature = self.features[img_name]
        feature = torch.FloatTensor(feature)
        caption = ['<sos>'] + caption + ['<eos>']
        caption = torch.LongTensor(vocab(caption))
        
        return feature, caption

    @staticmethod
    def collate_fn(batch):
        '''
        Return:
            features: (FloatTensor) sized [N, 256]
            captions: (LongTensor) sized [N, S]
            lengths: (LongTensor) sized [N]
        '''
        # sort batch data by caption length
        batch.sort(key=lambda x: len(x[1]), reverse=True)

        features, captions = zip(*batch)
        features = torch.stack(features, dim=0)
        lengths = torch.LongTensor(list(map(len, captions)))        
        captions = pad_sequence(captions, batch_first=True)

        return features, captions, lengths


if __name__ == '__main__':
    dataset = Dataset()
    loader = DataLoader(dataset, batch_size=32, collate_fn=Dataset.collate_fn)
    feature_b, caption_b, length_b = next(iter(loader))
    print(feature_b.size())
    print(caption_b.size())
    print(length_b.size())

    for caption, length in zip(caption_b, length_b):
        tokens = vocab.wordify(caption.numpy().tolist())
        print('{:2d} {}'.format(length, tokens))