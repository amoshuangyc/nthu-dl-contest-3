
class RunningAverage:
    def __init__(self):
        self.iter = 0
        self.val = 0

    def update(self, x):
        self.val = (self.val * self.iter + x) / (self.iter + 1)
        self.iter += 1

    def __str__(self):
        if self.iter == -1:
            return 'NaN'
        return '{:.4f}'.format(self.val)