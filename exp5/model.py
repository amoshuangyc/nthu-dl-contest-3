import torch
from torch import nn
from torch.nn import functional as F
from torch.nn.utils.rnn import pack_padded_sequence

from vocab import vocab


class Decoder(nn.Module):
    def __init__(self):
        super().__init__()
        self.embed = nn.Embedding(len(vocab), 256)
        self.lstm = nn.LSTM(256, 1024, dropout=0, num_layers=1, batch_first=True)
        self.fc = nn.Linear(1024, len(vocab))

    def forward(self, feature_b, caption_b, length_b):
        '''
        Args:
            (feature_b): (FloatTensor) sized [N, 256]
            (caption_b): (LongTensor) sized [N, S]
            (length_b): (LongTensor) sized [N]
        Return:
            (output_b): (PackedSequence) sized [sum(L), V]
        '''
        caption_b = self.embed(caption_b) # [N, S, 256]
        lstm_inp_b = (feature_b.unsqueeze(1), caption_b[:, :-1, :])
        lstm_inp_b = torch.cat(lstm_inp_b, dim=1) # [N, S, 256]
        lstm_inp_b = pack_padded_sequence(lstm_inp_b, length_b, batch_first=True) # [sum(L), 256]
        lstm_out_b, _ = self.lstm(lstm_inp_b) # [sum(L), 512]
        predict_b = self.fc(lstm_out_b.data) # [sum(L), V]
        return predict_b

    def evaluate(self, feature_b):
        '''
        Args:
            (feature_b): (FloatTensor) sized [N, 256]
        Return:
            (output_b): (LongTensor) sized [N, S]
        '''
        output_b = []
        lstm_inp_b = feature_b.unsqueeze(1) # [N, 1, 256]
        lstm_states = None

        for i in range(25):
            lstm_out_b, lstm_states = self.lstm(lstm_inp_b, lstm_states) # [N, 1, 512]
            predict_b = self.fc(lstm_out_b.squeeze(1)).argmax(dim=1) # [N]
            output_b.append(predict_b)

            lstm_inp_b = self.embed(predict_b) # [N, 256]
            lstm_inp_b = lstm_inp_b.unsqueeze(1) # [N, 1, 256]

        return torch.stack(output_b, dim=1) # [N, S]
