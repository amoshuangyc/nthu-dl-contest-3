import json
import random
import pandas as pd
from tqdm import tqdm
from pathlib import Path
from datetime import datetime
import matplotlib as mpl
mpl.use('svg')
import matplotlib.pyplot as plt
plt.style.use('seaborn')

import torch
from torch import nn
from torch.nn import functional as F
from torch.nn.utils.rnn import pack_padded_sequence
from torch.utils.data import Subset, ConcatDataset, DataLoader

import util
from vocab import vocab
from data import Dataset
from model import Decoder


device = 'cuda'
decoder = Decoder().to(device)
criterion = nn.CrossEntropyLoss().to(device)
optimizer = torch.optim.RMSprop(decoder.parameters(), lr=1e-3)

dataset = Dataset()
pivot = len(dataset) * 9 // 10
train_set = Subset(dataset, range(0, pivot))
valid_set = Subset(dataset, range(pivot, len(dataset)))
visul_set = ConcatDataset([
    Subset(train_set, random.sample(range(len(train_set)), 50)),
    Subset(valid_set, random.sample(range(len(valid_set)), 50)),
])
train_loader = DataLoader(train_set, batch_size=256, shuffle=True, collate_fn=Dataset.collate_fn)
valid_loader = DataLoader(valid_set, batch_size=256, collate_fn=Dataset.collate_fn)
visul_loader = DataLoader(visul_set, batch_size=256, collate_fn=Dataset.collate_fn)

log_dir = Path('./log/') / f'{datetime.now():%Y.%m.%d-%H:%M:%S}'
log_dir.mkdir(parents=True)


def train(pbar):
    metrics = {
        'loss': util.RunningAverage()
    }
    decoder.train()
    for feature_b, caption_b, length_b in iter(train_loader):
        feature_b = feature_b.to(device)
        caption_b = caption_b.to(device)
        length_b = length_b.to(device)

        target_b = pack_padded_sequence(caption_b, length_b, batch_first=True)
        target_b = target_b.data # [sum(L)]

        optimizer.zero_grad()
        predict_b = decoder(feature_b, caption_b, length_b) # [sum(L), V]
        loss = criterion(predict_b, target_b)
        loss.backward()
        optimizer.step()

        metrics['loss'].update(loss.item())
        pbar.set_postfix(metrics)
        pbar.update(len(feature_b))
    return metrics


def valid(pbar):
    metrics = {
        'loss': util.RunningAverage()
    }
    decoder.eval()
    for feature_b, caption_b, length_b in iter(valid_loader):
        feature_b = feature_b.to(device)
        caption_b = caption_b.to(device)
        length_b = length_b.to(device)

        target_b = pack_padded_sequence(caption_b, length_b, batch_first=True)
        target_b = target_b.data # [sum(L)]
        predict_b = decoder(feature_b, caption_b, length_b) # [sum(L), V]
        loss = criterion(predict_b, target_b)
        metrics['loss'].update(loss.item())
        pbar.set_postfix(metrics)
        pbar.update(len(feature_b))
    return {f'val_{k}':v for k, v in metrics.items()}


def visul(epoch, pbar):
    result = []
    for feature_b, caption_b, length_b in iter(visul_loader):
        predict_b = decoder.evaluate(feature_b.to(device)).cpu() # [N, S]
        for caption, predict in zip(caption_b, predict_b):
            result.append({
                'caption': ' '.join(vocab.wordify(caption.numpy().tolist())),
                'predict': ' '.join(vocab.wordify(predict.numpy().tolist()))
            })
            pbar.update(1)

    with (log_dir / f'{epoch:03d}.json').open('w') as f:
        json.dump(result, f, indent=2, ensure_ascii=False)


def log(epoch, train_metrics, valid_metrics):
    json_path = log_dir / 'log.json'
    if json_path.exists():
        df = pd.read_json(json_path)
    else:
        df = pd.DataFrame()

    metrics = {'epoch': epoch, **train_metrics, **valid_metrics}
    df = df.append(metrics, ignore_index=True)
    df = df.astype('str').astype('float')
    with json_path.open('w') as f:
        json.dump(df.to_dict(orient='records'), f, indent=2)

    fig, ax = plt.subplots(dpi=100)
    df[['loss', 'val_loss']].plot(kind='line', ax=ax)
    fig.savefig(str(log_dir / 'loss.svg'))
    plt.close()

    if df['loss'].idxmin() == epoch:
        torch.save(decoder,log_dir / 'model.pth')


for epoch in range(40):
    print('Epoch', epoch)
    with tqdm(total=len(train_set), desc='  Train', ascii=True) as pbar:
        train_metrics = train(pbar)
    with torch.no_grad():
        with tqdm(total=len(valid_set), desc='  Valid', ascii=True) as pbar:
            valid_metrics = valid(pbar)
        with tqdm(total=len(visul_set), desc='  Visul', ascii=True) as pbar:
            visul(epoch, pbar)
        log(epoch, train_metrics, valid_metrics)

